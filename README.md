# KiteFight
A 2 player kite fighting game developed in Adobe Flash ActionScript 3.0 as part of a 2 week Rapid Prototyping project at [FIEA](http://fiea.ucf.edu)

PLay the demo at http://nihavjain.info/demo/kitefite

###Goal:
Damage the kite of the other player until it drops down

###Controls: 
####Kite 1:
* Up: W;
* Down: S;
* Left: A;
* Right: D;

####Kite 2:
* Up: Up Arrow;
* Down: Down Arrow;
* Left: Left Arrow;
* Right: Right Arrow;

###Team:

####Producer: 
Agnesa Belegu
####Programmers: 
Nihav Jain, Brian Bennett
####Artists: 
Roselyn Troche, Summan Mirza
